# Standard Library Imports.
import os

# Setuptools Package Imports.
from setuptools import setup


# Open the README file for inclusion in the setup metadata.
README = open(os.path.join(os.path.dirname(__file__), 'README.md')).read()

# Allow setup.py to be run from any path.
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))


setup(
    name = 'NMCC-Calc-Dewpoint',
    version = '1',
    py_modules = ['cc.calc.dpnt'],
    entry_points = {
        'console_scripts': ['cc-calc-dewpoint = cc.calc.dpnt:main',]},
    license = 'BSD License',
    description = 'Calculate a dewpoint value.',
    long_description = README,
    url = 'https://bitbucket.org/notequal/nmcc-calc-dewpoint',
    author = 'Stanley Engle',
    author_email = 'sengle@nmsu.edu',
    classifiers = [
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.7',],)
