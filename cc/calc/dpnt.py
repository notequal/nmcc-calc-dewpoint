# Standard library imports.
import math


def calc(tair, relh, on_error = float('nan')):
    """Calculates the dewpoint from the temperature and RH.

    The dewpoint is calculated using the air temperature in degrees
    Celsius and the relative humidity.  See:
    https://en.wikipedia.org/wiki/Dew_point

    Parameters
    ----------
    tair : float
        The air temperature in degrees C.
    relh : float
        The relative humidity, as a percentage (0 to 100)
    on_error : optional, float('nan')
        The return value when an error occcurs during calculation.  May
        be any type, but preferably a float.

    Returns
    -------
    float
        The dewpoint in degrees C.

    """
    try:

        s1 = math.log(float(relh) / 100.0)
        s2 = (float(tair) * 17.625) / (float(tair) + 243.04)
        s3 = s1 + s2
        s4 = (17.625 - s1) - s2
        dpnt = round(243.04 * s3 / s4, 2)

    except:
        dpnt = on_error

    return dpnt


# #####################################################################
#
# #####################################################################

def main():

    # Command line parser.
    parser = argparse.ArgumentParser()

    parser.add_argument('tair',
        type = float,
        help = 'The air temperature in degrees Celsius')
    parser.add_argument('relh',
        type = float,
        help = 'The relative humidity as a percentage (0.0 to 100.0)')

    # Parse the command line arguments.
    args = parser.parse_args()

    # Get the heat index value and write it to standard out.
    sys.stdout.write(str(calc(args.tair, args.wspd)) + '\n')

    return None


if __name__ == '__main__':

    # Run from command line.
    main()

    # We're done!
    sys.exit(0)
